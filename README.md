# MapleStory-176-Server-Docker

## 免责申明
本项目来源于网络只为了学习交流之用，请不要用于任何商业目的，否则后果自负！
若由此引起的一切法律责任都与本人无关！版权都归游戏官方所有，请在文件下载后于24小时内删除！

#### 介绍
冒险岛-176-服务端-docker 一键构建
#### 软件架构
基于 swordie 编译
```docker
https://bitbucket.org/swordiemen/swordie.git
```
 也可以用我的加速镜像
 ```docker
https://gitee.com/xiaoyun461/swordie.git
```


#### 安装教程

```docker
docker-compose up -d
```
####  客户端(国际服)修改
1. 基于 [AuthHook-v176-StringPool](https://github.com/pokiuwu/AuthHook-v176-StringPool) 编译 生成 `ijl15.dll`

    也可以用我的加速镜像 `https://gitee.com/xiaoyun461/AuthHook-v176-StringPool`
2. 编译说明,需要 vs2019以上 v142
3. 打开项目 `Global.h` 修改 `OPT_ADDR_HOSTNAME` 为外网地址,本地则不变 `127.0.0.1`
4. 选择 Release X86 编译 生成 `ijl15.dll`
5. 复制到客户端根目录下
6. 在客户端目录下执行 cmd命令, 
  ```shell
MapleStory.exe GameLaunching 8.31.99.141 8484
  ```
   
   本地测试可直接把 项目下 `127.0.0.1/ijl15.dll` 复制到客户端
#### 使用说明
1. ```util/WZ.Dumper.1.9.3.zip``` 是为了 解析客户端*.wz文件

    [WZ-Dumper 项目地址](https://github.com/Xterminatorz/WZ-Dumper)

    假如要手工编译swordie项目,则把解析后的文件复制到项目根目录的WZ文件下

   (ps:没有WZ文件夹 请新建)

```shell
http://download2.nexon.net/Game/MapleStory/FullVersion/176/MSSetupv176.exe
http://download2.nexon.net/Game/MapleStory/FullVersion/176/MSSetupv176.z01
http://download2.nexon.net/Game/MapleStory/FullVersion/176/MSSetupv176.z02
http://download2.nexon.net/Game/MapleStory/FullVersion/176/MSSetupv176.z03
http://download2.nexon.net/Game/MapleStory/FullVersion/176/MSSetupv176.z04
http://download2.nexon.net/Game/MapleStory/FullVersion/176/MSSetupv176.z05
http://download2.nexon.net/Game/MapleStory/FullVersion/176/MSSetupv176.z06
```




