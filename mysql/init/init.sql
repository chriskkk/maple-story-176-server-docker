GRANT ALL PRIVILEGES ON *.* TO 'root'@'127.0.0.1' IDENTIFIED BY '123456' WITH GRANT OPTION;
FLUSH   PRIVILEGES;
GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost' IDENTIFIED BY '123456' WITH GRANT OPTION;
FLUSH   PRIVILEGES;
grant all privileges on *.* to 'root'@'%' identified by '123456';

create  database  `swordie`;

use `swordie`;

source /opt/sql/InitTable_equip_drops.sql;
source /opt/sql/InitTable_npc.sql;
source /opt/sql/InitTables_cashshop.sql;
source /opt/sql/InitTables_characters.sql;
source /opt/sql/InitTables_drops.sql;
source /opt/sql/InitTables_MonsterCollection.sql;
source /opt/sql/InitTables_shops.sql;