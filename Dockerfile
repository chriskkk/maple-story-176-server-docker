############## 下载git项目  #####################
FROM bitnami/git:2.34.0 as git-download
MAINTAINER biebbwa@163.com
ARG GITEE_HOME=https://gitee.com/xiaoyun461/swordie.git
WORKDIR /home
RUN git clone $GITEE_HOME

############## MySQL 数据库 #####################
FROM mysql:5.6 as mysql
RUN chmod +x /usr/local/bin/docker-entrypoint.sh
COPY mysql/init/init.sql /docker-entrypoint-initdb.d/
COPY --from=git-download  /home/swordie/sql/ /opt/sql/
RUN chown -R mysql:mysql /docker-entrypoint-initdb.d/
RUN chown -R mysql:mysql /opt/sql/

############## server-init 服务 #####################
FROM maven:3.8.3-openjdk-16-slim as server-init

ENV TZ=Asia/Shanghai
COPY --from=git-download /home/swordie /app
COPY config/settings.xml /usr/share/maven/conf/settings.xml
RUN  ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone \
     && cd /app \
     && mvn package -T 1C -DskipTests -Dmaven.compile.fork=true

############## server-base 服务 #####################
FROM server-init as server-base
ARG DB_HOST
ARG DB_PORT
ARG DB_USERNAME
ARG DB_PASSWORD
ARG DB_GAME_NAME

COPY config/hibernate.cfg.xml /app/src/main/java/hibernate.cfg.xml
## 修改 加载 Handler 方式
COPY fixbug/ChannelHandler.java /app/src/main/java/net/swordie/ms/connection/netty/ChannelHandler.java

RUN  cd /app \
     && sed -i "s#DB_HOST#${DB_HOST}#g" "/app/src/main/java/hibernate.cfg.xml" \
     && sed -i "s#DB_PORT#${DB_PORT}#g" "/app/src/main/java/hibernate.cfg.xml" \
     && sed -i "s#DB_USERNAME#${DB_USERNAME}#g" "/app/src/main/java/hibernate.cfg.xml" \
     && sed -i "s#DB_PASSWORD#${DB_PASSWORD}#g" "/app/src/main/java/hibernate.cfg.xml" \
     && sed -i "s#DB_GAME_NAME#${DB_GAME_NAME}#g" "/app/src/main/java/hibernate.cfg.xml" \
     && mvn clean package -T 1C -DskipTests -Dmaven.compile.fork=true


############## server #####################
FROM openjdk:16.0-jdk-slim as server-build
ARG DB_HOST
ARG DB_PORT
ENV DB_HOST=${DB_HOST}
ENV DB_PORT=${DB_PORT}
ENV JAR_NAME=maplestory-1.77.3-jar-with-dependencies.jar
ENV TZ=Asia/Shanghai
COPY --from=server-base /app/bin/${JAR_NAME} /app/
COPY --from=server-base /app/resources /app/resources/
COPY --from=server-base /app/scripts /app/scripts/
COPY wait-for-it.sh /usr/local/bin
RUN  chmod +x /usr/local/bin/wait-for-it.sh  \
     && ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

WORKDIR /app
EXPOSE 8483 8484
ENTRYPOINT  wait-for-it.sh ${DB_HOST}:${DB_PORT} -s -t 0 -- java -jar ${JAVA_OPTS} ${JAR_NAME} net.swordie.ms.Server


############## server 服务 基于 dat #####################
FROM server-build  as server-dat
ADD dat.tar.gz /app

############## server 服务 基于WZ #####################
FROM server-build  as server-wz
ARG WZ_PATH
ADD ${WZ_PATH} /app





